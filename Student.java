// Student.java

public class Student {
    // Fields 
    public String name;
    public int age;
    public double gpa; // Changed to private


    // Instance method: Study
    public void study() {
        System.out.println(name + " is studying for the upcoming test!");
    }

    // Instance method: Attend lecture
    public void attendLecture() {
        System.out.println(name + " is attending the lecture.");
    }
}
