
// Application.java

public class Application {
    public static void main(String[] args) {
        // Create two Student objects
        Student student1 = new Student();
        Student student2 = new Student();

        // Initialize their fields with different values
        student1.name = "Alice";
        student1.age = 20;
        student1.gpa = 3.8;

        student2.name = "Bob";
        student2.age = 22;
        student2.gpa = 3.5;
		
		Student[] section3 = new Student[3];
		section3[0] = student1;
		section3[1] = student2;
		section3[2] = new Student();
		
		section3[2].name = "Mark";
		section3[2].age = 21;
		section3[2].gpa = 3.1;
		
		System.out.println("Third student's name: " + section3[2].name);
        System.out.println("Third student's age: " + section3[2].age);
        System.out.println("Third student's GPA: " + section3[2].gpa);
    }
}
